### Paragraphs gantt
Paragraphs gantt gives the user the ability to easily add gantt chart via
paragraphs.

If you start with the paragraphs gantt demo then you will start creating a
<a href="/node/add/project">+ new project</a> After that you can add new gantt.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

Installation is as simple as copying the module into your 'modules' contrib
directory, then enabling the module.

1) Add fields in paragraphs.
2) Field date range, duration, links, parent, ...
3) Or install module demo and delete after that.
4) Mapping all fields in display manage.
