<?php

namespace Drupal\paragraphs_gantt\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphsTypeInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class definition for ComponentFormController.
 */
class ComponentFormController extends ControllerBase {

  /**
   * Responds with a component insert form.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\paragraphs\ParagraphsTypeInterface $paragraph_type
   *   The Paragraph Type to insert.
   * @param string $entity_type
   *   Entity type.
   * @param string $entity_field
   *   Entity field to store paragraphs.
   * @param int $entity_id
   *   Entity id.
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   *   A build array or Ajax response.
   */
  public function addForm(Request $request, ParagraphsTypeInterface $paragraph_type, $entity_type, $entity_field, $entity_id) {
    $response = new AjaxResponse();
    $modal_form = $this->formBuilder()->getForm('\Drupal\paragraphs_gantt\Form\AddComponentForm', $paragraph_type, $entity_type, $entity_field, $entity_id);
    $is_ajax = $request->isXmlHttpRequest();
    if (!$is_ajax) {
      return $modal_form;
    }
    $settings = ['width' => '80%'];
    $selector = '#paragraphs_gantt';
    $response->addCommand(new OpenDialogCommand($selector, $modal_form['#title'], $modal_form, $settings));
    return $response;
  }

  /**
   * Displays a paragraphs item.
   *
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The Paragraph item we are displaying.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function page(Paragraph $paragraph) {
    return [
      'paragraph' => $this->entityTypeManager()
        ->getViewBuilder('paragraph')
        ->view($paragraph),
    ];
  }

}
