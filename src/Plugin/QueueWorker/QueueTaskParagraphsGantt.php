<?php

namespace Drupal\paragraphs_gantt\Plugin\QueueWorker;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto end date.
 *
 * @QueueWorker(
 *   id = "queue_task_paragraphs_gantt",
 *   title = @Translation("Queue task paragraphs gantt"),
 *   cron = {"time" = 360}
 * )
 */
class QueueTaskParagraphsGantt extends QueueWorkerBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Query POST.
   *
   * @var mixed
   */
  protected $post;

  /**
   * The user takes the action.
   *
   * @var object
   */
  protected $currentUser;

  /**
   * Constructs a WorkTimekeeper instance.
   *
   * @param array $configuration
   *   The configuration for the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The plugin translation.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTags
   *   The service cache tags.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected EntityTypeManagerInterface $entityTypeManager, protected TranslationInterface $string_translation, protected CacheTagsInvalidatorInterface $cacheTags) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('string_translation'),
      $container->get('cache_tags.invalidator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->post = $data['data'];
    $this->currentUser = $this->entityTypeManager->getStorage('user')->load($data['current_user']);
    $this->actionTask($data['action'], $data['entity'], $data['paragraph_type'], $data['entity_field'], $data['field_settings']);
  }

  /**
   * Process Task.
   */
  protected function actionTask($action, $entity, $paragraph_type, $entity_field, $field_settings) {

    $output = ['action' => $action];
    $paragraph = Paragraph::load($this->post["id"]);
    if (empty($paragraph)) {
      return $output;
    }

    $fieldDefinitions = $paragraph->getFieldDefinitions();

    // Timezone current user.
    $user_timezone = $this->currentUser->getTimeZone() ?? "UTC";
    $time_timezone = new \DateTimeZone($user_timezone);
    $utc_timezone = $user_timezone == 'UTC' ? $time_timezone : new \DateTimeZone('UTC');

    // Type date actually, planned.
    $type_date = $this->getTypeDateField($fieldDefinitions, $field_settings);

    // Set start date.
    $actual_date = $this->processDate('start_date', 'end_date', $type_date, $field_settings, $time_timezone, $utc_timezone);
    // Set plan date.
    $planned_date = $this->processDate('planned_start', 'planned_end', $type_date, $field_settings, $time_timezone, $utc_timezone);

    // Set actual date and plan date.
    if (!empty($actual_date)) {
      if ($type_date['start_date_actually']['field_type'] == 'daterange') {
        $paragraph->set($field_settings['start_date'], $actual_date);
      }
      else {
        if (!empty($field_settings['start_date'])) {
          $paragraph->set($field_settings['start_date'], $actual_date['value']);
        }
        if (!empty($field_settings['end_date'])) {
          $paragraph->set($field_settings['end_date'], $actual_date['end_value']);
        }
      }
      if (!empty($field_settings['planned_date'])) {
        if ($type_date['start_date_planned']['field_type'] == 'daterange') {
          $paragraph->set($field_settings['planned_date'], $planned_date);
        }
        else {
          if (!empty($field_settings['planned_date'])) {
            $paragraph->set($field_settings['planned_date'], $planned_date['value']);
          }
          if (!empty($field_settings['planned_end_date'])) {
            $paragraph->set($field_settings['planned_end_date'], $planned_date['end_value']);
          }
        }
      }
    }

    // Update schedule planned.
    if (!empty($this->post['listSchedulePlanned'])) {
      $this->updateSchedulePlanned($field_settings, $type_date, $time_timezone, $utc_timezone);
    }

    // Set field option.
    $options = [
      'type',
      'duration',
      'progress',
      'priority',
      'planned_duration',
      'custom_field',
    ];
    foreach ($options as $option) {
      $value = $this->post[$option] ?? NULL;
      if (!empty($field_settings[$option])&& !empty($fieldDefinitions[$field_settings[$option]])) {
        $paragraph->set($field_settings[$option], $value);
      }
    }
    // Set text.
    if (!empty($field_settings['text']) && !empty($fieldDefinitions[$field_settings['text']])) {
      $paragraph->set($field_settings['text'], $this->post['text'] ?? 'Undefined');
    }
    // Set parent.
    if (!empty($field_settings['parent']) && !empty($fieldDefinitions[$field_settings['parent']])) {
      $paragraph->set($field_settings['parent'], $this->post['parent'] ?? NULL);
    }
    // Set open.
    if (!empty($field_settings['open']) && !empty($fieldDefinitions[$field_settings['open']])) {
      $paragraph->set($field_settings['open'], $this->post['open'] ?? 0);
    }
    // Set order.
    if (!empty($field_settings['order']) && !empty($fieldDefinitions[$field_settings['order']])) {
      $paragraph->set($field_settings['order'], $this->post['order'] ?? 0);
    }
    // Set resource.
    if (!empty($field_settings["custom_resource"])) {
      foreach ($field_settings["custom_resource"] as $field_resource) {
        if ($field_settings["creator"] == $field_resource || empty($fieldDefinitions[$field_resource])) {
          continue;
        }
        $resource = [];
        $this->post[$field_resource] = !empty($this->post[$field_resource]) ? json_decode($this->post[$field_resource]) : [];
        if (is_array($this->post[$field_resource])) {
          foreach ($this->post[$field_resource] as $resource_id) {
            if (!empty($resource_id)) {
              $resource[] = ['target_id' => $resource_id];
            }
          }
        }
        else {
          $resource = [['target_id' => $this->post[$field_resource]]];
        }
        $paragraph->set($field_resource, $resource);
      }
    }
    // Set constraint.
    if (!empty($field_settings["constraint"]) && !empty($this->post['constraint_type']) && !empty($fieldDefinitions[$field_settings['constraint']])) {
      $paragraph->set($field_settings["constraint"], [
        'first' => $this->post['constraint_type'] ?? NULL,
        'second' => $this->post['constraint_date'] ?? NULL,
      ]);
    }
    if ($action == 'inserted') {
      $paragraph->isNew();
    }
    $paragraph->save();
    $output['tid'] = $paragraph->id();

    if ($action == 'inserted') {
      $destination = $this->post['destination'] ?? '';
      $destination = str_replace('destination=', '', $destination);
      $destination = ['destination' => $destination];
      $output["link_detail"] = Url::fromRoute('paragraphs_gantt.edit', ['paragraph' => $paragraph->id()])
        ->setOptions(['query' => $destination])->toString();

      $fieldValue = $entity->get($entity_field)->getValue();
      $fieldValue[] = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
      $entity->set($entity_field, $fieldValue);
      $entity->save();
    }
    $this->cacheTags->invalidateTags($entity->getCacheTags());

    return $output;
  }

  /**
   * {@inheritDoc}
   */
  public function getTypeDateField($fieldDefinitions, $setting) {
    // Get type date field.
    $type_date = [
      'start_date_actually' => ['date_type' => '', 'field_type' => ''],
      'end_date_actually' => ['date_type' => '', 'field_type' => ''],
      'start_date_planned' => ['date_type' => '', 'field_type' => ''],
      'end_date_planned' => ['date_type' => '', 'field_type' => ''],
    ];
    $populateDateType = function ($settingKey, $typeKey) use (&$type_date, $fieldDefinitions, $setting) {
      if (!empty($setting[$settingKey]) && !empty($fieldDefinitions[$setting[$settingKey]])) {
        $dateSettings = $fieldDefinitions[$setting[$settingKey]]->getSettings();
        $type_date[$typeKey]['date_type'] = $dateSettings["datetime_type"];
        $type_date[$typeKey]['field_type'] = $fieldDefinitions[$setting[$settingKey]]->getType();
      }
    };
    $populateDateType('start_date', 'start_date_actually');
    $populateDateType('end_date', 'end_date_actually');
    $populateDateType('planned_date', 'start_date_planned');
    $populateDateType('planned_end_date', 'end_date_planned');

    return $type_date;
  }

  /**
   * Process Dates.
   *
   * {@inheritDoc}
   */
  private function processDate($start_key, $end_key, $type_date, $field_settings, $time_timezone, $utc_timezone, $start = NULL, $end = NULL) {
    $type_key_start = 'start_date_planned';
    $type_key_end = 'end_date_planned';
    if ($start_key == 'start_date') {
      $type_key_start = 'start_date_actually';
      $type_key_end = 'end_date_actually';
    }
    $dateFormatStart = $dateFormatEnd = 'Y-m-d\TH:i:s';
    if ($type_date[$type_key_start]['field_type'] == 'daterange' && $type_date[$type_key_start]['date_type'] == 'date') {
      $dateFormatStart = $dateFormatEnd = 'Y-m-d';
    }
    elseif ($type_date[$type_key_end]['date_type'] == 'date') {
      $dateFormatEnd = 'Y-m-d';
    }

    if (empty($start)) {
      $start = $this->post[$start_key];
    }
    if (empty($end)) {
      $end = $this->post[$end_key];
    }

    if (!empty($start) && $start != 'undefined') {
      $start_date = new DrupalDateTime($start, $time_timezone);
      $end_date = new DrupalDateTime($end, $time_timezone);

      if ($type_date[$type_key_start]['field_type'] == 'daterange' && $type_date[$type_key_start]['date_type'] == 'datetime') {
        $start_date->setTimezone($utc_timezone);
        $end_date->setTimezone($utc_timezone);
      }
      elseif ($type_date[$type_key_start]['date_type'] == 'datetime') {
        $start_date->setTimezone($utc_timezone);
        if (!empty($type_date[$type_key_end]['date_type']) && $type_date[$type_key_end]['date_type'] == 'datetime') {
          $end_date->setTimezone($utc_timezone);
        }
      }

      $type = $type_date[$type_key_start]["field_type"] == 'daterange' ? $type_date[$type_key_start]["date_type"] : $type_date[$type_key_end]["date_type"];
      if ($field_settings['last_of_the_day'] && $type == 'date') {
        $end_date->modify("-1 day");
      }

      return [
        'value' => $start_date->format($dateFormatStart),
        'end_value' => $end_date->format($dateFormatEnd),
      ];
    }
    return [
      'value' => NULL,
      'end_value' => NULL,
    ];
  }

  /**
   * Update schedule planned.
   *
   * {@inheritDoc}
   */
  protected function updateSchedulePlanned($field_settings, $type_date, $time_timezone, $utc_timezone) {
    $managerParagraph = $this->entityTypeManager->getStorage('paragraph');
    $listUpdate = Json::decode($this->post['listSchedulePlanned']);
    if (!empty($listUpdate)) {
      foreach ($listUpdate as $item_planned) {
        $entity_update = $managerParagraph->load($item_planned['id']);
        if ($entity_update) {
          $planned_date = $this->processDate('planned_start', 'planned_end', $type_date, $field_settings, $time_timezone, $utc_timezone, $item_planned['planned_start'], $item_planned['planned_end']);

          if ($type_date['start_date_planned']['field_type'] == 'daterange') {
            $entity_update->set($field_settings['planned_date'], $planned_date);
          }
          else {
            if (!empty($field_settings['planned_date'])) {
              $entity_update->set($field_settings['planned_date'], $planned_date['value']);
            }
            if (!empty($field_settings['planned_end_date'])) {
              $entity_update->set($field_settings['planned_end_date'], $planned_date['end_value']);
            }
          }
          $entity_update->save();
        }
      }
    }
  }

}
