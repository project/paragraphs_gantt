<?php

namespace Drupal\paragraphs_gantt\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the paragraphs gantt formatter.
 */
#[FieldFormatter(
  id: 'paragraphs_gantt_formatter',
  label: new TranslatableMarkup('Paragraphs gantt'),
  field_types: [
    'entity_reference_revisions',
  ],
)]
class ParagraphsGanttFormatter extends EntityReferenceFormatterBase {

  /**
   * The fields of paragraphs.
   *
   * @var array
   */
  protected array $fieldsParagraphs;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   The entity display repository.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity field manager.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathStack
   *   The current path stack.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selectionManager
   *   The entity reference selection plugin  manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   File Url Generator service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, protected EntityDisplayRepositoryInterface $entityDisplayRepository, protected DateFormatterInterface $dateFormatter, protected EntityFieldManagerInterface $entityFieldManager, protected EntityTypeManagerInterface $entityTypeManager, protected CurrentPathStack $currentPathStack, protected ModuleHandlerInterface $moduleHandler, protected SelectionPluginManagerInterface $selectionManager, protected AccountInterface $account, protected FileUrlGeneratorInterface $fileUrlGenerator, protected RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_display.repository'),
      $container->get('date.formatter'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('path.current'),
      $container->get('module_handler'),
      $container->get('plugin.manager.entity_reference_selection'),
      $container->get('current_user'),
      $container->get('file_url_generator'),
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    // Implement default settings.
    return [
      'view_mode' => 'default',
      'text' => 'field_gantt_text',
      'type' => 'field_gantt_type',
      'start_date' => 'field_gantt_date',
      'end_date' => 'field_gantt_end_date',
      'show_end' => FALSE,
      'duration' => 'field_gantt_duration',
      'progress' => 'field_gantt_progress',
      'open' => 'field_gantt_open',
      'parent' => 'field_gantt_parent',
      'links' => 'field_gantt_links',
      'order' => 'field_gantt_order',
      'custom_resource' => [],
      'planned_date' => 'field_gantt_planned_date',
      'planned_end_date' => 'field_gantt_planned_end_date',
      'planned_duration' => 'field_gantt_planned_duration',
      'custom_field' => '',
      'toolbar_text' => '',
      'add_task' => TRUE,
      'edit_task' => TRUE,
      'native_dialog' => FALSE,
      'work_time' => FALSE,
      'work_day' => [0],
      'send_email' => FALSE,
      'send_notification' => FALSE,
      'cdn' => TRUE,
      'holidays' => '01-01,01-05',
      'creator' => 'field_owner_paragraph',
      'permission_edit' => FALSE,
      'show_button_detail' => FALSE,
      'show_column_resource' => [],
      'show_lightbox_resource' => [],
      'group_resource' => [],
      'select_parent' => FALSE,
      'resource_has_edit' => ['field_owner_paragraph'],
      'last_of_the_day' => TRUE,
      'constraint' => '',
      'priority' => '',
      'hide_show_column' => FALSE,
      'column_buttons' => FALSE,
      'gantt_theme' => '',
      'hide_add_task_level' => FALSE,
      'hide_add_task_level_value' => 0,
      'control_bar' => ['auto_type'],
      'button_add_toolbar' => TRUE,
      'time_input_mode' => 'duration',
      'auto_schedule_planned' => FALSE,
      'calendar_ical' => '',
      'format_date' => '',
      'custom_format_date' => 'd/m/Y',
      'action_queue' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settingForm = [
      'view_mode' => [
        '#type' => 'select',
        '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type')),
        '#title' => $this->t('View mode'),
        '#default_value' => $this->getSetting('view_mode'),
        '#required' => TRUE,
      ],
      'text' => [
        '#title' => $this->t('Title'),
        '#description' => $this->t('Field title'),
        '#type' => 'select',
        '#required' => TRUE,
        '#options' => $this->getConfigurableFields([
          'list_integer',
          'list_string',
          'text',
          'text_long',
          'text_with_summary',
          'string',
          'string_long',
        ]),
        '#empty_option' => $this->t('- None -'),
        '#default_value' => $this->getSetting('text'),
      ],
      'start_date' => [
        '#title' => $this->t('Date'),
        '#description' => $this->t('This configuration is required and a duration or end date configuration must exist. If you select the date range field for this configuration, you can omit the duration and end date configurations.'),
        '#type' => 'select',
        '#required' => TRUE,
        '#options' => $this->getConfigurableFields(['datetime', 'daterange']),
        '#empty_option' => $this->t('- None -'),
        '#default_value' => $this->getSetting('start_date'),
      ],
      'format_date' => [
        '#title' => $this->t('Format date'),
        '#type' => 'select',
        '#required' => TRUE,
        '#options' => $this->getListDateFormat(),
        '#default_value' => $this->getSetting('format_date'),
      ],
      'custom_format_date' => [
        '#type' => 'textfield',
        '#title' => $this->t('Custom format date'),
        '#default_value' => $this->getSetting('custom_format_date'),
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][format_date]"]' => ['value' => 'custom'],
          ], [
            ':input[name="style_options[format_date]"]' => ['value' => 'custom'],
          ],
        ],
        '#description' => $this->t("Allowed characters: 'Y', 'm', 'd', 'H', 'i', 's', 'j', 'l', 'M', 'a', 'A', 'F', 'n', 'W', 'w', 'D', 'g', 'G', 'y', 'h'. If empty, the value 'd/m/Y' will be used"),
        '#multiple' => TRUE,
      ],
      'end_date' => [
        '#title' => $this->t('End date'),
        '#description' => $this->t('Link to Field actual end date'),
        '#type' => 'select',
        '#options' => $this->getConfigurableFields(['datetime']),
        '#empty_option' => $this->t('- None -'),
        '#default_value' => $this->getSetting('end_date'),
      ],
      'show_end' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Show end day'),
        '#default_value' => $this->getSetting('show_end'),
        '#description' => $this->t("If check it will add column end date."),
      ],
      'type' => [
        '#title' => $this->t('Type'),
        '#description' => $this->t('Link to Field type'),
        '#type' => 'select',
        '#required' => TRUE,
        '#options' => $this->getConfigurableFields(['list_string', 'string']),
        '#empty_option' => $this->t('- None -'),
        '#default_value' => $this->getSetting('type'),
      ],
      'duration' => [
        '#title' => $this->t('Duration'),
        '#description' => $this->t('Link to Field duration'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields(['integer']),
        '#default_value' => $this->getSetting('duration'),
      ],
      'time_input_mode' => [
        '#title' => $this->t('Time input mode'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => [
          'duration' => $this->t('Duration'),
          'end_time' => $this->t('End time'),
          'responsive' => $this->t('Responsive'),
        ],
        '#default_value' => $this->getSetting('time_input_mode'),
      ],
      'work_time' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Work day'),
        '#default_value' => $this->getSetting('work_time'),
        '#description' => $this->t("Enables calculating the duration of tasks in working time instead of calendar time."),
      ],
      'work_day' => [
        '#type' => 'select',
        '#title' => $this->t('Work day in week'),
        '#default_value' => $this->getSetting('work_day'),
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][work_time]"]' => ['checked' => TRUE],
          ], [
            ':input[name="style_options[work_time]"]' => ['checked' => TRUE],
          ],
        ],
        '#options' => [
          $this->t('Sunday'),
          $this->t('Monday'),
          $this->t('Tuesday'),
          $this->t('Wednesday'),
          $this->t('Thursday'),
          $this->t('Friday'),
          $this->t('Saturday'),
        ],
        '#description' => $this->t('Pick out non-working days of the week'),
        '#multiple' => TRUE,
      ],
      'holidays' => [
        '#title' => $this->t('Holidays'),
        '#description' => $this->t('Enter holidays separated by, Example "01-01,01-05,09-04-2023,... Format d-m or d-m-Y"'),
        '#type' => 'textarea',
        '#default_value' => $this->getSettings()['holidays'],
        '#states' => [
          'visible' =>
            [
              ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][work_time]"]' => ['checked' => TRUE],
            ],
          [
            ':input[name="style_options[work_time]"]' => ['checked' => TRUE],
          ],
        ],
      ],
      'progress' => [
        '#title' => $this->t('Progress'),
        '#description' => $this->t('Link to Field progress'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields([
          'float',
          'list_float',
          'decimal',
        ]),
        '#default_value' => $this->getSetting('progress'),
      ],
      'open' => [
        '#title' => $this->t('Open'),
        '#description' => $this->t('Link to Field open'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields(['boolean']),
        '#default_value' => $this->getSetting('open'),
      ],
      'parent' => [
        '#title' => $this->t('Parent'),
        '#description' => $this->t('Link to Field parent'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields([
          'integer',
          'list_integer',
        ]),
        '#default_value' => $this->getSetting('parent'),
      ],
      'links' => [
        '#title' => $this->t('Links type'),
        '#description' => $this->t('Link to Field link'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields([
          'double_field',
          'triples_field',
        ]),
        '#default_value' => $this->getSetting('links'),
      ],
      'order' => [
        '#title' => $this->t('Order'),
        '#description' => $this->t('Link to Field order'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields([
          'integer',
          'float',
          'decimal',
        ]),
        '#default_value' => $this->getSetting('order'),
      ],
      'custom_resource' => [
        '#title' => $this->t('Resource custom'),
        '#description' => $this->t('Link to Field Resource custom'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields([
          'entity_reference',
        ]),
        '#default_value' => $this->getSetting('custom_resource'),
        '#multiple' => TRUE,
      ],
      'show_column_resource' => [
        '#title' => $this->t('Column show resource'),
        '#description' => $this->t('Select the columns to be displayed'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields([
          'entity_reference',
        ]),
        '#default_value' => $this->getSetting('show_column_resource'),
        '#multiple' => TRUE,
      ],
      'show_lightbox_resource' => [
        '#title' => $this->t('Light box show resource'),
        '#description' => $this->t('Select the field to be displayed'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields([
          'entity_reference',
        ]),
        '#default_value' => $this->getSetting('show_lightbox_resource'),
        '#multiple' => TRUE,
      ],
      'priority' => [
        '#title' => $this->t('Priority'),
        '#description' => $this->t('Link to Field priority'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields([
          'entity_reference',
          'list_string',
        ]),
        '#default_value' => $this->getSetting('priority'),
      ],
      'creator' => [
        '#title' => $this->t('Author'),
        '#description' => $this->t('Link to Field author'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields(['entity_reference']),
        '#default_value' => $this->getSetting('creator'),
      ],
      'permission_edit' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Permission to edit on a task'),
        '#default_value' => $this->getSetting('permission_edit'),
        '#description' => $this->t("Only author can edit their task."),
      ],
      'resource_has_edit' => [
        '#title' => $this->t('Resource has edit'),
        '#description' => $this->t('Resource with editing rights'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields([
          'entity_reference',
        ]),
        '#default_value' => $this->getSetting('resource_has_edit'),
        '#multiple' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][permission_edit]"]' => ['checked' => TRUE],
          ],
        ],
      ],
      'add_task' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Add Task'),
        '#default_value' => $this->getSetting('add_task'),
        '#description' => $this->t("Enable possibility 'Add Task' from Gantt chart."),
      ],
      'hide_add_task_level' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Hide add task level'),
        '#default_value' => $this->getSetting('hide_add_task_level'),
        '#description' => $this->t("Hide the add tasks button for tasks whose level is greater than or equal to the input value."),
        '#states' => [
          'visible' => [
            ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][add_task]"]' => ['checked' => TRUE],
          ],
        ],
      ],
      'hide_add_task_level_value' => [
        '#type' => 'number',
        '#min' => '0',
        '#title' => $this->t('Hide add task level value'),
        '#default_value' => $this->getSetting('hide_add_task_level_value'),
        '#description' => $this->t("Hide the add tasks button for tasks whose level is greater than or equal to the input value."),
        '#states' => [
          'visible' => [
            [
              ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][add_task]"]' => ['checked' => TRUE],
              ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][hide_add_task_level]"]' => ['checked' => TRUE],
            ],
          ],
        ],
      ],
      'edit_task' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Edit Task'),
        '#default_value' => $this->getSetting('edit_task'),
        '#description' => $this->t("Enable possibility 'Edit Task' from Gantt chart."),
      ],
      'show_button_detail' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Show button details'),
        '#default_value' => $this->getSetting('show_button_detail'),
        '#description' => $this->t("Show details task with drupal form"),
      ],
      'send_email' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Send an email after the change of status'),
        '#default_value' => $this->getSetting('send_email'),
      ],
      'cdn' => [
        '#title' => $this->t('Use CDN'),
        '#description' => $this->t('If not you can use custom version in libraries'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('cdn'),
      ],
      'last_of_the_day' => [
        '#title' => $this->t('Last of the day'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('last_of_the_day'),
      ],
      'custom_field' => [
        '#title' => $this->t('Custom field'),
        '#description' => $this->t('Additional column'),
        '#type' => 'select',
        '#empty_option' => $this->t('- None -'),
        '#options' => $this->getConfigurableFields(['integer']),
        '#default_value' => $this->getSetting('custom_field'),
      ],
      'calendar_ical' => [
        '#title' => $this->t('Custom text at the modal'),
        '#description' => $this->t('Variable available {{ paragraph_name }}, {{ paragraph_type }}, {{ entity_type }}, {{ entity_field }}, {{ entity_id }}'),
        '#type' => 'textarea',
        '#default_value' => $this->getSettings()['calendar_ical'],
      ],
      'toolbar_text' => [
        '#title' => $this->t('Custom text at the toolbar'),
        '#description' => $this->t('Variable available {{ paragraph_name }}, {{ paragraph_type }}, {{ entity_type }}, {{ entity_field }}, {{ entity_id }}'),
        '#type' => 'textarea',
        '#default_value' => $this->getSettings()['toolbar_text'],
      ],
      'button_add_toolbar' => [
        '#title' => $this->t('Button add on tool barr'),
        '#description' => $this->t('Enable this option to add a create task button to the toolbar'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('button_add_toolbar'),
      ],
      'baseline' => [
        '#type' => 'details',
        '#title' => $this->t('Base line (version PRO)'),
        '#description' => $this->t('If you want show double gantt to compare actual vs planned date. This method is available only in Pro versions'),

        'group_resource' => [
          '#title' => $this->t('Group resource'),
          '#type' => 'select',
          '#empty_option' => $this->t('- None -'),
          '#options' => $this->getConfigurableFields([
            'entity_reference',
          ]),
          '#default_value' => $this->getSetting('group_resource'),
          '#multiple' => TRUE,
          '#parents' => [
            'fields',
            $this->fieldDefinition->getName(),
            'settings_edit_form',
            'settings',
            'group_resource',
          ],
        ],
        'planned_date' => [
          '#type' => 'select',
          '#title' => $this->t('Planned Date field'),
          '#empty_option' => $this->t('- None -'),
          '#options' => $this->getConfigurableFields(['datetime', 'daterange']),
          '#default_value' => $this->getSetting('planned_date'),
          '#description' => $this->t("This configuration is required and a duration or end date configuration must exist. If you select the date range field for this configuration, you can omit the duration and end date configurations."),
          '#parents' => [
            'fields',
            $this->fieldDefinition->getName(),
            'settings_edit_form',
            'settings',
            'planned_date',
          ],
        ],
        'planned_end_date' => [
          '#type' => 'select',
          '#title' => $this->t('End Planned Date field'),
          '#empty_option' => $this->t('- None -'),
          '#options' => $this->getConfigurableFields(['datetime']),
          '#default_value' => $this->getSetting('planned_end_date'),
          '#description' => $this->t("Link to Field planned end date."),
          '#parents' => [
            'fields',
            $this->fieldDefinition->getName(),
            'settings_edit_form',
            'settings',
            'planned_end_date',
          ],
        ],
        'planned_duration' => [
          '#title' => $this->t('Duration'),
          '#description' => $this->t('Planned duration field'),
          '#type' => 'select',
          '#empty_option' => $this->t('- None -'),
          '#options' => $this->getConfigurableFields(['integer']),
          '#default_value' => $this->getSetting('planned_duration'),
          '#states' => [
            'visible' => [
              ':input[name="fields[' . $this->fieldDefinition->getName() . '][settings_edit_form][settings][planned_date]"]' => ['!value' => ''],
            ], [
              ':input[name="style_options[planned_date]"]' => ['!value' => ''],
            ],
          ],
        ],
        'constraint' => [
          '#title' => $this->t('Constraint'),
          '#description' => $this->t('Link to Field constraint.'),
          '#type' => 'select',
          '#empty_option' => $this->t('- None -'),
          '#parents' => [
            'fields',
            $this->fieldDefinition->getName(),
            'settings_edit_form',
            'settings',
            'constraint',
          ],
          '#options' => $this->getConfigurableFields([
            'double_field',
            'triples_field',
          ]),
          '#default_value' => $this->getSetting('constraint'),
        ],
        'auto_schedule_planned' => [
          '#title' => $this->t('Auto schedule planned'),
          '#description' => $this->t('Automatic scheduling for scheduled dates'),
          '#type' => 'checkbox',
          '#default_value' => $this->getSetting('auto_schedule_planned'),
          '#parents' => [
            'fields',
            $this->fieldDefinition->getName(),
            'settings_edit_form',
            'settings',
            'auto_schedule_planned',
          ],

        ],
      ],
    ];

    $settingForm['select_parent'] = [
      '#title' => $this->t('Change parent in lightbox'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('select_parent'),
    ];
    $settingForm['hide_show_column'] = [
      '#title' => $this->t('Hide show column'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('hide_show_column'),
      '#description' => $this->t('Hide and show columns with right mouse click'),
    ];
    $settingForm['column_buttons'] = [
      '#title' => $this->t('Column buttons'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('column_buttons'),
      '#description' => $this->t('Display buttons as the same column'),
    ];
    $settingForm['gantt_theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Gantt theme'),
      '#default_value' => $this->getSetting('gantt_theme'),
      '#empty_option' => $this->t('- None -'),
      '#options' => [
        'broadway' => $this->t('Broadway'),
        'contrast_black' => $this->t('Contrast black'),
        'contrast_white' => $this->t('Contrast white'),
        'material' => $this->t('Material'),
        'meadow' => $this->t('Meadow'),
        'skyblue' => $this->t('Skyblue'),
        'terrace' => $this->t('Terrace'),
      ],
    ];
    $settingForm['control_bar'] = [
      '#title' => $this->t('Control bar'),
      '#description' => $this->t('The following options will be displayed to customize the gantt chart'),
      '#type' => 'checkboxes',
      '#options' => $this->controlBar(),
      '#default_value' => $this->getSetting('control_bar'),
      '#multiple' => TRUE,
    ];
    if ($this->moduleHandler->moduleExists('pwa_firebase')) {
      $settingForm['send_notification'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Send notification to assign user'),
        '#default_value' => $this->getSetting('send_notification'),
      ];
    }
    $settingForm['native_dialog'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Custom form'),
      '#default_value' => $this->getSetting('native_dialog'),
      '#description' => $this->t("Use native Drupal form to add/edit."),
    ];

    $settingForm['action_queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Action queue'),
      '#default_value' => $this->getSetting('action_queue'),
      '#description' => $this->t("Task update action added to queue for processing."),
    ];
    return $settingForm + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    if (empty($this->fieldsParagraphs)) {
      $this->setFieldsParagraphs();
    }
    $summary = parent::settingsSummary();
    if (!empty($this->getSetting('text'))) {
      $summary[] = $this->t('Field title: @field', ['@field' => $this->getLabelName($this->getSetting('text'))]);
    }
    if (!empty($this->getSetting('start_date'))) {
      $summary[] = $this->t('Field date: @field', ['@field' => $this->getLabelName($this->getSetting('start_date'))]);
    }
    if (!empty($this->getSetting('duration'))) {
      $summary[] = $this->t('Field duration: @field', ['@field' => $this->getLabelName($this->getSetting('duration'))]);
    }
    if (!empty($this->getSetting('progress'))) {
      $summary[] = $this->t('Field progress: @field', ['@field' => $this->getLabelName($this->getSetting('progress'))]);
    }
    if (!empty($this->getSetting('open'))) {
      $summary[] = $this->t('Field open: @field', ['@field' => $this->getLabelName($this->getSetting('open'))]);
    }
    if (!empty($this->getSetting('parent'))) {
      $summary[] = $this->t('Field parent: @field', ['@field' => $this->getLabelName($this->getSetting('parent'))]);
    }
    if (!empty($this->getSetting('dynamic_progress'))) {
      $summary[] = $this->t('Dynamic progress summary: @field', ['@field' => $this->getLabelName($this->getSetting('dynamic_progress'))]);
    }
    return $summary;
  }

  /**
   * Get list of fields.
   */
  protected function getConfigurableFields($type = FALSE) {
    if (empty($this->fieldsParagraphs)) {
      $this->setFieldsParagraphs();
    }
    $listField = [];
    foreach ($this->fieldsParagraphs as $field_name => $field) {
      $listField[$field_name] = $field->getLabel();
      if (!empty($type)) {
        if (!is_array($type) && $type != $field->getType()) {
          unset($listField[$field_name]);
        }
        if (is_array($type) && !in_array($field->getType(), $type)) {
          unset($listField[$field_name]);
        }
      }
    }
    return $listField;
  }

  /**
   * {@inheritdoc}
   */
  protected function setFieldsParagraphs() {
    $targetType = $this->getFieldSetting('target_type');
    $targetBundle = array_key_first($this->fieldDefinition->getSetting("handler_settings")["target_bundles"]);
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraphs_entity */
    $paragraphs_entity = $this->entityTypeManager->getStorage($targetType)
      ->create(['type' => $targetBundle]);
    $field_definitions = $paragraphs_entity->getFieldDefinitions();
    foreach ($field_definitions as $field_name => $field) {
      if ($field instanceof FieldConfigInterface || $field instanceof FieldConfig) {
        $this->fieldsParagraphs[$field_name] = $field;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getLabelName($fieldName) {
    if (!empty($this->fieldsParagraphs[$fieldName])) {
      return $this->fieldsParagraphs[$fieldName]->getLabel();
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $setting = $this->getSettings();
    $targetType = $this->getFieldSetting('target_type');
    $field_definition = $items->getFieldDefinition();
    $entity_type_id = $field_definition->get('entity_type');
    $selectionHandler = $this->selectionManager->getSelectionHandler($field_definition);
    $bundles = $selectionHandler->entityTypeBundleInfo->getBundleInfo($targetType);
    $field_name = $field_definition->getName();
    $field_label = $field_definition->getLabel();
    $entity = $items->getEntity();
    $entityId = $entity->id();
    $currentURL = $this->currentPathStack->getPath();
    $destination = ['destination' => $currentURL];
    $view_mode = $this->viewMode == 'full' ? 'default' : $this->viewMode;
    $dialog_width = '80%';
    $handlers = $field_definition->getSetting("handler_settings");
    $handler = array_key_first($handlers['target_bundles']);
    $storage = $this->entityTypeManager->getStorage('paragraphs_type');
    $paragraphs_type = $storage->load($handler);

    // Get option control bar.
    $control_bar = $setting["control_bar"] ?? [];
    foreach ($control_bar as &$valueBar) {
      $valueBar = !empty($valueBar);
    }
    // Get list creator resource.
    $listSource = [];
    $list_source = [];
    // Field definitions, entity.
    $paragraph = Paragraph::create(['type' => $handler]);
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions('paragraph', $handler);

    // Get list priority resource.
    if (!empty($setting['priority'])) {
      $field_type_priority = $fieldDefinitions[$setting['priority']]->getType();
      if ($field_type_priority == 'list_string') {
        $listSource[$setting['priority']][$setting['priority']] = $fieldDefinitions[$setting['priority']]->getSettings()['allowed_values'];
      }
      else {
        $listSource[$setting['priority']] = $this->selectionManager
          ->getSelectionHandler($fieldDefinitions[$setting['priority']], $paragraph)
          ->getReferenceableEntities();
      }
    }

    // Get list creator resource.
    if (!empty($setting['creator'])) {
      $listSource[$setting['creator']] = $this->selectionManager
        ->getSelectionHandler($fieldDefinitions[$setting['creator']], $paragraph)
        ->getReferenceableEntities();
    }

    // Get list custom resource.
    if (!empty($setting['custom_resource'])) {
      foreach ($setting['custom_resource'] as $field) {
        $listSource[$field] = $this->selectionManager
          ->getSelectionHandler($fieldDefinitions[$field], $paragraph)
          ->getReferenceableEntities();
      }
    }

    // Merge resource.
    if (!empty($listSource)) {
      foreach ($listSource as $field => $bundleLinks) {
        $list_source[$field] = [
          'label' => $fieldDefinitions[$field]->getLabel(),
          'data' => [],
        ];
        if (!empty($bundleLinks)) {
          foreach ($bundleLinks as $bundle => $data) {
            if (!empty($listSource[$field][$bundle])) {
              foreach ($listSource[$field][$bundle] as $id => $name) {
                if ($id == 0) {
                  continue;
                }
                $list_source[$field]['data'][] = [
                  'key' => $id,
                  'label' => strip_tags($name),
                ];
              }
            }
          }
        }
      }
    }

    // Get config field.
    $validateField = [];
    if (!empty($fieldDefinitions[$setting['text']])) {
      $validateField['text'] = $this->getValidateField($fieldDefinitions[$setting['text']], $paragraph);
    }

    if (!empty($fieldDefinitions[$setting['custom_field']])) {
      $validateField['custom_field'] = $this->getValidateField($fieldDefinitions[$setting['custom_field']], $paragraph);
    }

    if (!empty($fieldDefinitions[$setting['priority']])) {
      $validateField['priority'] = $this->getValidateField($fieldDefinitions[$setting['priority']], $paragraph);
    }

    if (!empty($fieldDefinitions[$setting['constraint']])) {
      $validateField['constraint'] = $this->getValidateField($fieldDefinitions[$setting['constraint']], $paragraph);
    }

    if (!empty($fieldDefinitions[$setting['planned_date']])) {
      $validateField['time_planned'] = $this->getValidateField($fieldDefinitions[$setting['planned_date']], $paragraph);
    }

    if (!empty($fieldDefinitions[$setting['start_date']])) {
      $validateField['time'] = $this->getValidateField($fieldDefinitions[$setting['start_date']], $paragraph);
    }

    if (!empty($setting['custom_resource'])) {
      foreach ($setting['custom_resource'] as $resource) {
        if (!empty($fieldDefinitions[$resource])) {
          $validateField['custom_resource'][$resource] = $this->getValidateField($fieldDefinitions[$resource], $paragraph);
        }
      }
    }

    // Get type date field.
    $type_date = [
      'start_date_actually' => [
        'date_type' => '',
        'field_type' => '',
      ],
      'end_date_actually' => [
        'date_type' => '',
        'field_type' => '',
      ],
      'start_date_planned' => [
        'date_type' => '',
        'field_type' => '',
      ],
      'end_date_planned' => [
        'date_type' => '',
        'field_type' => '',
      ],
    ];

    $format_date_actually = 'date';
    $format_date_planned = 'date';

    if (!empty($setting['start_date']) && !empty($fieldDefinitions[$setting['start_date']])) {
      $dateSettings = $fieldDefinitions[$setting['start_date']]->getSettings();
      $dateType = $dateSettings["datetime_type"];
      $fieldType = $fieldDefinitions[$setting['start_date']]->getType();
      if ($dateType == 'datetime') {
        $format_date_actually = $dateType;
      }
      $type_date['start_date_actually']['date_type'] = $dateType;
      $type_date['start_date_actually']['field_type'] = $fieldType;
    }

    if (!empty($setting['end_date']) && !empty($fieldDefinitions[$setting['end_date']])) {
      $dateSettings = $fieldDefinitions[$setting['end_date']]->getSettings();
      $dateType = $dateSettings["datetime_type"];
      $fieldType = $fieldDefinitions[$setting['end_date']]->getType();
      $fieldTypeStart = $fieldDefinitions[$setting['start_date']]->getType();
      if ($dateType == 'datetime' && $fieldTypeStart != 'daterange') {
        $format_date_actually = $dateType;
      }
      $type_date['end_date_actually']['date_type'] = $dateType;
      $type_date['end_date_actually']['field_type'] = $fieldType;
    }

    if (!empty($setting['planned_date']) && !empty($fieldDefinitions[$setting['planned_date']])) {
      $dateSettings = $fieldDefinitions[$setting['planned_date']]->getSettings();
      $dateType = $dateSettings["datetime_type"];
      $fieldType = $fieldDefinitions[$setting['planned_date']]->getType();
      if ($dateType == 'datetime') {
        $format_date_planned = $dateType;
      }
      $type_date['start_date_planned']['date_type'] = $dateType;
      $type_date['start_date_planned']['field_type'] = $fieldType;
    }

    if (!empty($setting['planned_end_date']) && !empty($fieldDefinitions[$setting['planned_end_date']])) {
      $dateSettings = $fieldDefinitions[$setting['planned_end_date']]->getSettings();
      $dateType = $dateSettings["datetime_type"];
      $fieldType = $fieldDefinitions[$setting['planned_end_date']]->getType();
      $fieldTypeStart = $fieldDefinitions[$setting['planned_date']]->getType();
      if ($dateType == 'datetime' && $fieldTypeStart != 'daterange') {
        $format_date_planned = $dateType;
      }
      $type_date['end_date_planned']['date_type'] = $dateType;
      $type_date['end_date_planned']['field_type'] = $fieldType;
    }

    // Custom field.
    if (!empty($setting['custom_field']) && !empty($fieldDefinitions[$setting['custom_field']])) {
      $setting['custom_field_name'] = $fieldDefinitions[$setting['custom_field']]->getLabel();
    }

    // Format date.
    if ($setting['format_date'] == 'custom') {
      $setting['format_date'] = !empty($setting['custom_format_date']) ? $setting['custom_format_date'] : 'd/m/Y';
    }
    $date_format = str_replace(
      ['Y', 'm', 'd', 'H', 'i', 's', 'j', 'l', 'M', 'a', 'A', 'F', 'n', 'W', 'w', 'D', 'g', 'G', 'y', 'h'],
      [
        '%Y',
        '%m',
        '%d',
        '%H',
        '%i',
        '%s',
        '%j',
        '%l',
        '%M',
        '%a',
        '%A',
        '%F',
        '%n',
        '%W',
        '%w',
        '%D',
        '%g',
        '%G',
        '%y',
        '%h',
      ],
      $setting['format_date']
    );

    $path = '';
    if (method_exists($paragraphs_type, 'getIconUrl')) {
      $path = $paragraphs_type->getIconUrl();
    }
    $title = implode(' ', [
      '<span class="bi bi-calendar-plus"><b class="d-none">📅</b></span>',
      $this->t('Add'),
      $bundles[$handler]['label'],
    ]);
    if (!empty($path)) {
      $title = '<img src="' . $path . '" height="25"/> ' . $title;
      $field_label = '<img src="' . $path . '" height="25"/> ' . $field_label;
    }
    $ganttId = $field_name . '_' . $handler . '_' . $entityId;
    $attr = [
      'class' => ['gantt', $handler],
      'id' => Html::getId($ganttId),
    ];
    $lib = ['paragraphs_gantt/paragraphs-gantt-main' . (!empty($setting['cdn']) ? '.cdn' : '')];
    if (!empty($setting['custom_resource']) || !empty($setting['creator'])) {
      $lib[] = 'paragraphs_gantt/paragraphs-gantt.chosen';
    }
    $base = !empty($setting["gantt_theme"]) ? $setting["gantt_theme"] : 'base';
    $lib[] = "paragraphs_gantt/gantt-theme-$base" . (!empty($setting['cdn']) ? '.cdn' : '');

    $data = $this->convertGanttData($items, $langcode, $setting, $type_date, $destination);
    $context = [
      'paragraph_type' => $handler,
      'entity_type' => $entity_type_id,
      'entity_field' => $field_name,
      'entity_id' => $entityId,
      'paragraph_name' => $bundles[$handler]['label'],
    ];
    $add_link = Url::fromRoute('paragraphs_gantt.add', $context)
      ->setOptions(['query' => $destination]);
    $ajax_url = Url::fromRoute('paragraphs.gantt', [
      'paragraph_type' => $handler,
      'view_mode' => $view_mode,
      'entity_type' => $entity_type_id,
      'entity_field' => $field_name,
      'entity_id' => $entityId,
    ]);
    // Convert holiday to format Y-m-d.
    if (!empty($setting['holidays'])) {
      $current_year = date("Y");
      $new_date_array = [];
      $holidays = explode(',', str_replace([' ', PHP_EOL], [
        '',
        ',',
      ], $setting['holidays']));
      foreach ($holidays as $holiday) {
        $extract = explode('-', $holiday);
        $day = $extract[0];
        $month = $extract[1];
        $year = $extract[2] ?? $current_year;
        $new_date_array[] = date('Y-m-d', strtotime("$year-$month-$day"));
      }
      $setting['holidays'] = $new_date_array;
    }
    // Get patch css.
    $module_path = $this->moduleHandler->getModule('paragraphs_gantt')
      ->getPath();
    $arrSource = [
      $module_path,
      'css',
      'reset.css',
    ];
    $link_css = implode(DIRECTORY_SEPARATOR, $arrSource);
    $link_css = $this->fileUrlGenerator->generateAbsoluteString($link_css);

    if (!empty($setting['calendar_ical'])) {
      $calendar_ical = [
        '#type' => 'inline_template',
        '#template' => $setting['calendar_ical'],
        '#context' => $context,
      ];

      $calendar_ical = $this->renderer->render($calendar_ical);
    }

    $elements = [
      '#id' => $ganttId,
      '#id_gantt' => "gantt_$ganttId",
      '#ajax' => $ajax_url,
      '#add' => [
        '#type' => 'link',
        '#url' => $add_link,
        '#title' => Markup::create($title),
        '#attributes' => [
          'class' => ['use-ajax', 'btn', 'btn-default'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode(['width' => $dialog_width]),
        ],
      ],
      '#attributes' => new Attribute($attr),
      "#paragraph_parent_id" => $entityId,
      "#paragraph_parent_type" => $entity_type_id,
      "#paragraph_parent_field_name" => $field_name,
      '#toolbar_text' => [
        '#type' => 'inline_template',
        '#template' => $setting['toolbar_text'],
        '#context' => $context,
      ],
      '#button_add_toolbar' => !empty($setting['button_add_toolbar']),
      '#settings' => $setting,
      '#type' => 'container',
      '#theme' => 'paragraphs_gantt_wrapper',
      '#field_name' => $field_name,
      '#title' => $field_label,
      '#attached' => [
        'drupalSettings' => [
          'current_user' => $this->account->id(),
          'gantt' => [
            $ganttId => [
              'id' => 'gantt_' . $ganttId,
              'control_bar' => $control_bar,
              'control_bar_label' => $this->controlBar(),
              'server_list_resource' => $list_source,
              'permission_edit' => !empty($setting['permission_edit']),
              'show_button_detail' => $setting['show_button_detail'],
              'add_task' => !empty($setting["add_task"]),
              'add_link' => $add_link->toString(),
              'parent_field' => $setting['parent'] ?? NULL,
              'edit_task' => !empty($setting["edit_task"]),
              'native_dialog' => !empty($setting["native_dialog"]),
              'work_time' => !empty($setting['work_time']),
              'work_day' => $setting['work_day'] ?? [],
              'holidays' => $setting['holidays'] ?? [],
              'creator' => $setting['creator'] ?? NULL,
              'setting_resource' => [
                'resource' => $setting['custom_resource'] ?? [],
                'resource_column' => $setting['show_column_resource'] ?? [],
                'resource_lightbox' => $setting['show_lightbox_resource'] ?? [],
                'resource_has_edit' => $setting['resource_has_edit'] ?? [],
                'resource_group' => $setting['group_resource'] ?? [],
              ],
              'has_duration' => $setting['duration'] ?? NULL,
              'time_input_mode' => $setting['time_input_mode'] ?? NULL,
              'start_date' => $setting['start_date'] ?? NULL,
              'end_date' => $setting['end_date'] ?? NULL,
              'planned_date' => $setting['planned_date'] ?? NULL,
              'planned_end_date' => $setting['planned_end_date'] ?? NULL,
              'show_end' => !empty($setting['show_end']),
              'auto_schedule_planned' => !empty($setting['auto_schedule_planned']),
              'custom_field' => $setting['custom_field'] ?? NULL,
              'custom_field_name' => $setting['custom_field_name'] ?? NULL,
              'current_path' => 'destination=' . $this->currentPathStack->getPath(),
              'ajax' => $ajax_url->toString(),
              'use_cdn' => !empty($setting['cdn']),
              'select_parent' => !empty($setting['select_parent']),
              'hide_add_task_level' => !empty($setting['hide_add_task_level']),
              'hide_add_task_level_value' => $setting['hide_add_task_level_value'] ?? 0,
              'column_buttons' => !empty($setting['column_buttons']),
              'last_of_the_day' => !empty($setting['last_of_the_day']),
              'order' => $setting['order'] ?? NULL,
              'constraint' => $setting['constraint'] ?? NULL,
              'priority' => $setting['priority'] ?? NULL,
              'hide_show_column' => !empty($setting['hide_show_column']),
              'validate_field' => $validateField,
              'format_date_actually' => $format_date_actually,
              'format_date_planned' => $format_date_planned,
              'type_date' => $type_date,
              'gantt_title' => method_exists($entity, 'getTitle') ? $entity->getTitle() : 'Gantt-' . $entity->id(),
              'link_css' => $link_css,
              'date_format' => $date_format,
              'data' => $data,
              'link_detail' => [
                'display' => Url::fromRoute('paragraphs_gantt.display', ['paragraph' => "-arg_0-"])
                  ->setOptions(['query' => $destination])
                  ->toString(),
                'edit' => Url::fromRoute('paragraphs_gantt.edit', ['paragraph' => "-arg_0-"])
                  ->setOptions(['query' => $destination])
                  ->toString(),
              ],
              'import' => Url::fromRoute('paragraphs.import', [
                'paragraph_type' => $handler,
                'view_mode' => $view_mode,
                'entity_type' => $entity_type_id,
                'entity_field' => $field_name,
                'entity_id' => $entityId,
              ])->toString(),
              'calendar_ical' => $calendar_ical ?? '',
            ],
          ],
        ],
        'library' => $lib,
      ],
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function convertGanttData(FieldItemListInterface $items, string $langcode, array $setting, $type_date = [], $destination = NULL) {

    $entities = array_values($this->getEntitiesToView($items, $langcode));
    // When a display renderer doesn't exist, fall back to the default.
    $field_definition = $items->getFieldDefinition();
    $handlers = $field_definition->getSetting("handler_settings");
    $targetBundle = array_key_first($handlers['target_bundles']);
    $links = [];
    $outData = [];
    $mappingGantt = [
      'text',
      'type',
      'start_date',
      'duration',
      'progress',
      'open',
      'parent',
      'planned_date',
      'planned_duration',
      'priority',
      'constraint',
      'custom_field',
      'creator',
      'custom_resource',
      'order',
    ];

    $user_timezone = $this->account->getTimeZone() ?? "UTC";
    $time_timezone = new \DateTimeZone($user_timezone);

    foreach ($entities as $delta => $entity) {
      if ($targetBundle != $entity->bundle()) {
        continue;
      }

      $data[$delta] = ['id' => $entity->id()];
      foreach ($mappingGantt as $ganttField) {
        if (!is_array($setting[$ganttField]) && !empty($field_name = $setting[$ganttField]) && !empty($entity->$field_name)) {
          $value = $entity->get($field_name)->getString();
          if (!empty($value)) {
            $data[$delta][$ganttField] = $value;
          }

          // Format date.
          $format = 'Y-m-d H:i';

          // Process start date.
          if ($ganttField == 'start_date' && !$entity->get($field_name)->isEmpty()) {
            $dateValueStart = new DrupalDateTime($entity->get($field_name)->value, 'UTC');
            if ($type_date['start_date_actually']['date_type'] == 'datetime') {
              $dateValueStart->setTimezone($time_timezone);
            }
            $data[$delta]['start_date'] = $dateValueStart->format($format);
            if ($type_date["start_date_actually"]["field_type"] !== 'daterange' && !empty($setting['end_date'])) {
              if (!$entity->get($setting['end_date'])->isEmpty()) {
                $dateValueEnd = new DrupalDateTime($entity->get($setting['end_date'])->value, 'UTC');
                if ($type_date['end_date_actually']['date_type'] == 'datetime') {
                  $dateValueEnd->setTimezone($time_timezone);
                }
              }
              else {
                $dateValueEnd = clone $dateValueStart;
              }
            }
            else {
              $dateValueEnd = new DrupalDateTime($entity->get($field_name)->end_value, 'UTC');
              if ($type_date['start_date_actually']['date_type'] == 'datetime') {
                $dateValueEnd->setTimezone($time_timezone);
              }
            }
            $data[$delta]['end_date'] = $dateValueEnd->format($format);

            // Option Last of the day.
            $type = $type_date["start_date_actually"]["field_type"] == 'daterange' ? $type_date["start_date_actually"]["date_type"] : $type_date["end_date_actually"]["date_type"];
            if ($setting['last_of_the_day'] && $type == 'date') {
              $dateValueEnd->modify('+1 day');
              $data[$delta]['end_date'] = $dateValueEnd->format($format);
            }
          }

          // Process planned date.
          if ($ganttField == 'planned_date' && !$entity->get($field_name)->isEmpty()) {
            if (!$entity->get($field_name)->isEmpty()) {
              $dateValueStart = new DrupalDateTime($entity->get($field_name)->value, 'UTC');
              if ($type_date['start_date_planned']['date_type'] == 'datetime') {
                $dateValueStart->setTimezone($time_timezone);
              }
              $data[$delta]['planned_start'] = $dateValueStart->format($format);
              if ($type_date["start_date_planned"]["field_type"] !== 'daterange' && !empty($setting['planned_end_date'])) {
                if (!$entity->get($setting['planned_end_date'])->isEmpty()) {
                  $dateValueEnd = new DrupalDateTime($entity->get($setting['planned_end_date'])->value, 'UTC');
                  if ($type_date['end_date_planned']['date_type'] == 'datetime') {
                    $dateValueEnd->setTimezone($time_timezone);
                  }
                }
                else {
                  $dateValueEnd = clone $dateValueStart;
                }
              }
              else {
                $dateValueEnd = new DrupalDateTime($entity->get($field_name)->end_value, 'UTC');
                if ($type_date['start_date_planned']['date_type'] == 'datetime') {
                  $dateValueEnd->setTimezone($time_timezone);
                }
              }
              $data[$delta]['planned_end'] = $dateValueEnd->format($format);

              // Option Last of the day.
              $type = $type_date["start_date_planned"]["field_type"] == 'daterange' ? $type_date["start_date_planned"]["date_type"] : $type_date["end_date_planned"]["date_type"];
              if ($setting['last_of_the_day'] && $type == 'date') {
                $dateValueEnd->modify('+1 day');
                $data[$delta]['planned_end'] = $dateValueEnd->format($format);
              }
            }
          }

          // Process duration.
          if ($ganttField == 'duration') {
            $data[$delta][$ganttField] = !empty($value) ? $value : 1;
          }

          // Process parent.
          if ($ganttField == 'parent') {
            $data[$delta][$ganttField] = !empty($value) ? $value : '0';
          }
          // Process order.
          if ($ganttField == 'order') {
            $data[$delta][$ganttField] = !empty($value) ? (float) $value : 0;
          }
          // Process creator.
          if ($ganttField == 'creator') {
            $data[$delta][$ganttField] = [];
            $creators = !$entity->get($field_name)
              ->isEmpty() ? $entity->get($field_name)->getValue() : [];
            if (!empty($creators)) {
              $data[$delta][$ganttField] = array_column($creators, 'target_id');
            }
          }
          // Process custom field.
          if ($ganttField == 'custom_field') {
            $data[$delta]['custom_field'] = $value;
          }
        }

        // Process resource.
        if ($ganttField == 'custom_resource') {
          foreach ($setting[$ganttField] as $item) {
            $data[$delta][$item] = [];
            if (!$entity->get($item)->isEmpty()) {
              $values = $entity->get($item)->getValue();
              foreach ($values as $value) {
                if (!empty($value->entity)) {
                  $data[$delta][$item][] = $value->entity->id();
                }
                elseif (is_array($value) && !empty($value['target_id'])) {
                  $data[$delta][$item][] = $value['target_id'];
                }
              }
            }
          }
        }

        // Link detail of task.
        if ($setting['show_button_detail']) {
          $data[$delta]["link_detail"] = Url::fromRoute('paragraphs_gantt.edit', ['paragraph' => $entity->id()])
            ->setOptions(['query' => $destination])->toString();
        }

        // Process priority.
        if ($ganttField == 'priority' && !empty($field_name = $setting[$ganttField]) && !$entity->get($field_name)->isEmpty()) {
          $data[$delta][$ganttField] = [];
          $values = $entity->get($setting[$ganttField])->getValue();
          foreach ($values as $value) {
            if (!empty($value->entity)) {
              $data[$delta][$ganttField] = $value->entity->id();
            }
            elseif (is_array($value) && (!empty($value['target_id']) || !empty($value['value']))) {
              $data[$delta][$ganttField] = $value['target_id'] ?? $value['value'];
            }
          }
        }

        // Process constraint.
        if ($ganttField == 'constraint' && !empty($field_name = $setting[$ganttField]) && !$entity->get($field_name)->isEmpty()) {
          $valueConstraint = $entity->get($setting[$ganttField])->getValue();
          if ($valueConstraint) {
            $data[$delta]['constraint_type'] = $valueConstraint[0]['first'] ?? NULL;
            $data[$delta]['constraint_date'] = $valueConstraint[0]['second'] ?? NULL;
          }
        }
      }

      // Resource edit.
      if (!empty($setting['permission_edit']) && !empty($setting['resource_has_edit']) && !in_array($this->account->id(), $data[$delta]['creator'])) {
        $data[$delta]['readonly'] = TRUE;
        $data[$delta]["link_detail"] = Url::fromRoute('paragraphs_gantt.display', ['paragraph' => $entity->id()])
          ->setOptions(['query' => $destination])->toString();
        if (!empty($data[$delta]['readonly'])) {
          foreach ($setting['resource_has_edit'] as $field_resource) {
            $resource_target = $entity->get($field_resource)->getValue();
            $resource_target = array_column($resource_target, 'target_id');
            if (in_array($this->account->id(), $resource_target)) {
              $data[$delta]['readonly'] = FALSE;
              $data[$delta]["link_detail"] = Url::fromRoute('paragraphs_gantt.edit', ['paragraph' => $entity->id()])
                ->setOptions(['query' => $destination])->toString();
              break;
            }
          }
        }
      }

      // Process Links.
      if (!empty($setting['links'])) {
        $valueLinks = $entity->get($setting['links'])->getValue();
        $entityTarget = [];
        if (!empty($valueLinks)) {
          $entityTarget = Paragraph::loadMultiple(array_column($valueLinks, 'first'));
        }

        if (!empty($entityTarget)) {
          foreach (array_values($entityTarget) as $index => $entity_target) {
            $links[$delta] = [
              'id' => implode('-', [$entity->id(), $entity_target->id()]),
              'source' => (int) $entity->id(),
              'target' => (int) $entity_target->id(),
              'type' => $valueLinks[$index]['second'],
              'lag' => is_numeric($valueLinks[$index]['third']) ? $valueLinks[$index]['third'] : NULL,
            ];
            // Resource edit.
            if (!empty($setting['permission_edit']) && !empty($setting['resource_has_edit'])) {
              $links[$delta]['readonly'] = !empty($data[$delta]['readonly']);
            }
          }
        }
      }
    }
    if (!empty($data)) {
      $outData['data'] = array_values($data);
      if (!empty($links)) {
        $outData['links'] = array_values($links);
      }
    }
    return $outData;
  }

  /**
   * Get connfig field.
   *
   * {@inheritDoc}
   */
  private function getValidateField($field_definitions, $paragraph) {
    $type = $field_definitions->getType();
    $storageField = $field_definitions->getFieldStorageDefinition();
    $defaultValue = $field_definitions->getDefaultValue($paragraph);
    $validateField = [
      'label' => $field_definitions->getLabel(),
      'cardinality' => $storageField->getCardinality(),
      'required' => $field_definitions->isRequired(),
      'default_value' => NULL,
    ];

    if (in_array($type, [
      'integer',
      'float',
      'decimal',
    ])) {
      $validateField['min'] = $storageField->getSettings()["min"] ?? NULL;
      $validateField['max'] = $storageField->getSettings()["max"] ?? NULL;
    }

    if (!empty($defaultValue) && in_array($type, ['entity_reference'])) {
      $validateField['default_value'] = array_column($defaultValue, 'target_id') ?? NULL;
    }

    if (!empty($defaultValue) && in_array($type, [
      'double_field',
      'triples_field',
    ])) {
      $validateField['default_value']['first'] = $defaultValue[0]['first'] ?? NULL;
      $validateField['default_value']['second'] = $defaultValue[0]['second'] ?? NULL;
      $validateField['default_value']['third'] = $defaultValue[0]['third'] ?? NULL;
    }

    if (!empty($defaultValue) && in_array($type, [
      'datetime',
      'daterange',
    ])) {
      $user_timezone = $this->account->getTimeZone() ?? "UTC";
      $time_timezone = new \DateTimeZone($user_timezone);
      $date = $type == 'daterange' ? $defaultValue[0]["start_date"] : $defaultValue[0]["date"];
      if (!empty($date)) {
        $date->setTimezone($time_timezone);
        $validateField['default_value']['start'] = $date->format('Y-m-d\TH:i:s') ?? NULL;
        $date->modify('+1 minute');
        $validateField['default_value']['end'] = $date->format('Y-m-d\TH:i:s') ?? NULL;
      }
    }
    if (!empty($defaultValue) && empty($validateField['default_value'])) {
      $validateField['default_value'] = $defaultValue[0]['value'];
    }
    return $validateField;
  }

  /**
   * Control bar options.
   *
   * {@inheritDoc}
   */
  private function controlBar() {
    return [
      'round_dnd_dates' => $this->t('Allows task start and end dates to be rounded to the smallest unit'),
      'show_column_wbs' => $this->t('Show column WBS'),
      'lock_completed_task' => $this->t('Limit editing of completed tasks'),
      'dynamic_progress' => $this->t('Dynamic progress summary'),
      'progress_text' => $this->t('Text progress'),
      'auto_type' => $this->t('Auto type - Pro version'),
      'auto_schedule' => $this->t('Auto schedule - Pro version'),
      'click_drag' => $this->t('Enables advanced drag-n-drop - Pro version'),
      'critical_path' => $this->t('Shows the critical path in the chart - Pro version'),
      'drag_project' => $this->t('Drag and drop of line - Pro version'),
      'hide_weekend_scale' => $this->t('Hide weekend scale - Pro version'),
      'highlight_drag_task' => $this->t('Highlights drag task - Pro version'),
      'show_slack' => $this->t('Show slack - Pro version'),
    ];

  }

  /**
   * Return date format system options.
   *
   * {@inheritDoc}
   */
  private function getListDateFormat() {
    $result = [];
    $dateFormatStorage = $this->entityTypeManager->getStorage('date_format');
    $drupalDateFormat = $dateFormatStorage->loadMultiple();
    foreach ($drupalDateFormat as $format) {
      $result[$format->getPattern()] = $format->label();
    }
    $result += ['custom' => $this->t('Custom')];
    return $result;
  }

}
