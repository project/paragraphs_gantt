<?php

namespace Drupal\paragraphs_gantt\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDefaultWidget;

/**
 * Plugin implementation of the 'daterange_default' widget.
 */
#[FieldWidget(
  id: 'duration_default',
  label: new TranslatableMarkup('Duration date and time range'),
  field_types: ['daterange'],
)]
class DurationField extends DateRangeDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    if (!empty($this->getSetting('field_duration'))) {
      $element["value"]['#attributes']['class'][] = 'calculate-duration start-date';
      $element["value"]['#attributes']['data-field-duration'] = $this->getSetting('field_duration');
      $element["end_value"]['#attributes']['class'][] = 'end-date';
      $element["end_value"]['#attributes']['data-field-duration'] = $this->getSetting('field_duration');
      $element['#attached']['library'][] = 'paragraphs_gantt/paragraphs-gantt.duration';
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'field_duration' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $options = [];

    $entity_type = $form["#entity_type"];
    $bundle = $form["#bundle"];
    // @phpstan-ignore-next-line
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fieldDefinitions = $entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    $userInput = $form_state->getUserInput();
    $typSupport = ['number', 'decimal', 'float'];
    if (!empty($userInput["fields"])) {
      foreach ($userInput["fields"] as $field_name => $field_widget) {
        if (!empty($field_widget['type']) &&
          in_array($field_widget['type'], $typSupport)) {
          $options[$field_name] = (string) $fieldDefinitions[$field_name]->getLabel();
        }
      }
    }
    $elements['field_duration'] = [
      '#type' => 'select',
      '#options' => $options,
      '#empty_option' => $this->t('- Select -'),
      '#title' => $this->t('Duration field'),
      '#default_value' => $this->getSetting('field_duration'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if (!empty($this->getSetting('field_duration'))) {
      $summary[] = $this->t('Duration field: @duration', [
        '@duration' => $this->getSetting('field_duration'),
      ]);
    }
    return $summary;
  }

}
