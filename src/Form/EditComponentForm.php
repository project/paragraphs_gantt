<?php

namespace Drupal\paragraphs_gantt\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Paragraph Edit Form class.
 */
class EditComponentForm extends ContentEntityForm {

  /**
   * Constructs a paragraphs edit form object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module Handler service.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTags
   *   The cache service.
   * @param \Drupal\content_translation\ContentTranslationManagerInterface|null $translationManager
   *   The translation manager.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, protected LanguageManagerInterface $languageManager, ModuleHandlerInterface $moduleHandler, protected CacheTagsInvalidatorInterface $cacheTags, protected ?ContentTranslationManagerInterface $translationManager) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->setModuleHandler($moduleHandler);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('language_manager'),
      $container->get('module_handler'),
      $container->get('cache_tags.invalidator'),
      $container->has('content_translation.manager') ? $container->get('content_translation.manager') : NULL,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'paragraphs_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return $this->getFormId();
  }

  /**
   * {@inheritdoc}
   *
   * Overridden to store the root parent entity.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?EntityInterface $paragraph = NULL) {
    if (empty($this->entity) && !empty($paragraph)) {
      $this->setEntity($paragraph);
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $entity_parent = $this->entity->getParentEntity();
    if (!empty($entity_parent)) {
      $this->cacheTags->invalidateTags($entity_parent->getCacheTags());
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function init(FormStateInterface $form_state) {
    if ($this->entity->isTranslatable()) {
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
      $form_state->set('langcode', $langcode);

      if (!$this->entity->hasTranslation($langcode)) {
        $translationSource = $this->entity;
        $parentEntity = $this->entity->getParentEntity();
        $parentSourceLangCode = $parentEntity->language()->getId();
        if ($parentEntity->hasTranslation($langcode)) {
          $parentEntity = $parentEntity->getTranslation($langcode);
          $parentSourceLangCode = $this->translationManager->getTranslationMetadata($parentEntity)->getSource();
        }

        if ($this->entity->hasTranslation($parentSourceLangCode)) {
          $translationSource = $this->entity->getTranslation($parentSourceLangCode);
        }

        $this->entity = $this->entity->addTranslation($langcode, $translationSource->toArray());
        $this->translationManager->getTranslationMetadata($this->entity)->setSource($translationSource->language()->getId());
      }
    }

    parent::init($form_state);
  }

  /**
   * The _title_callback for the paragraphs_item.view route.
   *
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The current paragraphs_item.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(Paragraph $paragraph) {
    return $this->entityRepository->getTranslationFromContext($paragraph)->label() . ' #' . $paragraph->id();
  }

}
